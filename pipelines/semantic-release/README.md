# Semantic Release `.gitlab-ci.yml` template

Hidden job template for setting up automatic releases in Gitlab CI.

To include the latest version in your CI pipeline add the following job and a [masked `GITLAB_TOKEN` or `GL_TOKEN` CI variable](https://github.com/semantic-release/gitlab#environment-variables) with at least `api` access:

```yaml
# ...
include:
  remote: "https://gitlab.com/cividi/devops/pipelines/-/raw/main/pipelines/semantic-release/semantic-release.base.gitlab-ci.yml"
# ...
release:
  extends: .release:auto
  needs: []
# ...
```

or pin a specfic version

```yaml
# ...
include:
  remote: "https://gitlab.com/cividi/devops/pipelines/-/raw/v1.0.0/pipelines/semantic-release/semantic-release.base.gitlab-ci.yml"
# ...
release:
  extends: .release:auto
  needs: []
# ...
```

## Recommendations

- protect the `main` branch, only allow pushes via merge requests
