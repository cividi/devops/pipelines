# Contributing

## Git Commit Guidelines

This project uses [Semantic Versioning](https://semver.org). Commit messages should adhere to
the conventions of [Conventional Commits (v1.0.0)](https://www.conventionalcommits.org/en/v1.0.0/).

### TL;DR

- Commit messages starting with `fix: ` trigger a patch version bump
- Commit messages starting with `feat: ` trigger a minor version bump
- Commit messages including `BREAKING CHANGE: ` in the footer and or a `!` after the type/scope (e.g. `feat(api)!: `) trigger a major version bump.
- Commit messages can be scoped with adding brackets, e.g. `feat(frontend): ` scopes the feature to a frontend section (e.g. in a genrerated changelog)
- Other options (based on the [Angular convention](https://github.com/angular/angular/blob/22b96b9/CONTRIBUTING.md#-commit-message-guidelines)) for prefixes: `docs: `, `style: `, `test: `, `refactor: `, `build`, `ci: `, `chore: `, `perf: `

## Automatic versioning

If [configured](https://handbook.cividi.io/infrastructure/version_control#configure-automatic-versioning), each push to `main` can trigger a [`semantic-release`](https://semantic-release.gitbook.io/semantic-release/)
CI job that determines and pushes a new version tag (if any) based on the
last version tagged and the new commits pushed. Notice that this means that if a
Merge Request contains, for example, several `feat: ` commits, only one minor
version bump will occur on merge. If your Merge Request includes several commits
you may prefer to ignore the prefix on each individual commit and instead add
an empty commit sumarizing your changes like so:

```
git commit --allow-empty -m '[BREAKING CHANGE|feat|fix]: <changelog summary message>'
```
