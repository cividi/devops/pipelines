# cividi pipeline templates

## What is this?

A collection of cividi pipeline templates - mostly [`.gitlab-ci.yml`](https://docs.gitlab.com/ee/ci/yaml/).

## Contributing

Please see [CONTRIBUTING](CONTRIBUTING.md).

## License

See [LICENSE](LICENSE.md)
